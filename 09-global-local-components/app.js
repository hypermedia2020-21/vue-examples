Vue.component('my-global-component', {
    data() {
      return{
        title: 'Hello, I am a global component'
      }
      }
        
});
  


const myLocalComponent = {
    data() {
      return {
        title: 'Hello, I am a local component',
      };
    },  
  };
  

  
new Vue({
    el: '#app',
    name: 'vue-instance',
    components: {
      myLocalComponent: myLocalComponent,  
    },
    data() {
      return {
        greeting: 'Hello people!', 
        question: 'Whats up?',
      };
    },
  });