"use strict";
Vue.config.errorHandler = function (err, vm, info) {
  // handle error
  // `info` is a Vue-specific error info, e.g. which lifecycle hook
  // the error was found in. Only available in 2.2.0+
  console.error(err, info);
};

Vue.component("button-counter", {
  template:
    '<div><button @click="decrement" class="button is-info ">Decrement from Component</button> <span class="tag is-warning  is-medium">{{ text }}</span></div>',
  props: {
    text: {
      type: Number,
      default: 1
    }
  },
  methods: {
    decrement: function() {
      //this.counter++
      this.$emit("decrement");
    }
  }
});

Vue.component("child", {
  props: {
    text: {
      type: Number,
      default: 1
    }
  },
  template: "#test",
  methods: {
    increment: function() {
      this.$emit("increment");
    },
    
  }
});

new Vue({
  el: "#app",
  data: {
    total: 1
  },
  methods: {
    incrementTotal: function() {
      this.total++;
    },
    decrementTotal: function() {
      this.total--;
    }
  }
});

